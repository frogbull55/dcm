import bpy
from bpy.types import AddonPreferences
from bpy.props import StringProperty

class Prefs(AddonPreferences):
    # this must match the add-on name, use '__package__'
    # when defining this in a submodule of a python package.
    bl_idname = __package__

    exported_meshes_path: StringProperty(
        name="Exported Meshes Path",
        subtype='FILE_PATH',
    )

    def draw(self, context):
        layout = self.layout
        layout.prop(self, "exported_meshes_path")