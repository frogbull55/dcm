# When bpy is already in local, we know this is not the initial import...
if "bpy" in locals():
    # ...so we need to reload our submodule(s) using importlib
    import importlib
    importlib.reload(prefs)
    importlib.reload(export_mesh)
    importlib.reload(export_selected_meshes)

import bpy

bl_info = {
    "name": "DCMesh Exporter",
    "author": "Ross Kilgariff",
    "version": (1, 0, 0),
    "blender": (3, 4, 1),
    "description": "Export .dcmesh files for use in independent Dreamcast games",
    "location": "Addons Preferences",
    "warning": "",
    "category": "Import-Export"
}

from . import prefs
from . import export_mesh
from . import export_selected_meshes

operator_classes = [
    export_mesh.DCMeshExporter,
    export_selected_meshes.ExportSelectedMeshesOperator,
    prefs.Prefs
]

register_operators, unregister_operators = bpy.utils.register_classes_factory(operator_classes)

def register():
    register_operators()

def unregister():
    unregister_operators()